//
//  AppNavigator.swift
//  Weather
//
//  Created by Sergey Tszyu on 29/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

struct AppNavigator {
    
    static func setupRootViewController(for window: UIWindow?) {
        
        if Defaults.isTutorialShown.boolValue {
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                case .denied, .notDetermined, .restricted:
                    let navigationController = NavigationController(rootViewController: R.storyboard.intro.locationIntroViewController()!)
                    window?.rootViewController = navigationController
                default:
                    if Defaults.notificationConfirmed.boolValue {
                        window?.rootViewController = R.storyboard.weather.instantiateInitialViewController()
                    } else {
                        let navigationController = NavigationController(rootViewController: R.storyboard.intro.notificationsIntroViewController()!)
                        window?.rootViewController = navigationController
                    }
                }
            }
        } else {
            window?.rootViewController = R.storyboard.intro.instantiateInitialViewController()
        }
        window?.makeKeyAndVisible()
    }

    static func setRootViewController(_ controller: UIViewController?, animated: Bool = true) {
        
        if let window = UIApplication.shared.keyWindow, let controller = controller {
            
            if animated {
                UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: {
                    window.rootViewController = controller
                }) { _ in }
            } else {
                window.rootViewController = controller
            }
        }
    }
}
