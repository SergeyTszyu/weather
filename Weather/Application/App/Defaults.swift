//
//  Defaults.swift
//  Weather
//
//  Created by Sergey Tszyu on 29/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import Foundation

enum Defaults: String {
    
    case isTutorialShown
    case notificationConfirmed
    
    var boolValue: Bool {
        return UserDefaults.standard.bool(forKey: rawValue)
    }
    
    var intValue: Int {
        return UserDefaults.standard.integer(forKey: rawValue)
    }
    
    func set(_ value: Bool) {
        UserDefaults.standard.set(value, forKey: rawValue)
    }
    
    func set(_ value: Int) {
        UserDefaults.standard.set(value, forKey: rawValue)
    }
}
