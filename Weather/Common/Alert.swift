//
//  Alert.swift
//  Weather
//
//  Created by Sergey Tszyu on 30/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

struct Alert {
    
    enum AlertType {
        
        case success
        case error
        case custom(title: String)
        
        var title: String {
            switch self {
            case .success:
                return "Success"
            case .error:
                return "Error"
            case .custom(let title):
                return title
            }
        }
    }
    
    static func presentAlertView(withType type: AlertType, message: String?, actions: [UIAlertAction]? = nil, from controller: UIViewController? = UIApplication.topViewController()) {
        
        let alert = UIAlertController(title: type.title, message: message ?? "", preferredStyle: UIAlertController.Style.alert)
        
        if let actions = actions, actions.count > 0 {
            
            for action in actions {
                alert.addAction(action)
            }
            
        } else {
            let okAction = UIAlertAction(title: "Ok",
                                         style: UIAlertAction.Style.default,
                                         handler: nil)
            alert.addAction(okAction)
        }
        
        controller?.present(alert, animated: true, completion: nil)
    }
    
    static func showSettings(from controller: UIViewController? = UIApplication.topViewController()) {
        
        let message = "Allow \"Weather\" to Have Access to Your Location"
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let settings = UIAlertAction(title: "Settings", style: .default, handler: { _ in
            UIApplication.openSettings()
        })
        alert.addAction(settings)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            controller?.dismiss(animated: true, completion: nil)
        })
        alert.addAction(cancel)
        
        controller?.present(alert, animated: true, completion: nil)
    }
    
    static func showNotificationSettings(from controller: UIViewController? = UIApplication.topViewController()) {
        
        let message = "Allow \"Weather\" to Have Access send you notifications"
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let settings = UIAlertAction(title: "Settings", style: .default, handler: { _ in
            UIApplication.openSettings()
        })
        alert.addAction(settings)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            controller?.dismiss(animated: true, completion: nil)
        })
        alert.addAction(cancel)
        
        controller?.present(alert, animated: true, completion: nil)
    }
}
