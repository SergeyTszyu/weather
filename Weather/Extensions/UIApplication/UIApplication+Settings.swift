//
//  UIApplication+Settings.swift
//  Weather
//
//  Created by Sergey Tszyu on 30/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

extension UIApplication {
    
    static func openSettings() {
        
        guard let url = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
