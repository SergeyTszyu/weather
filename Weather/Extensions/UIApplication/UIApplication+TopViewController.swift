//
//  UIApplication+TopViewController.swift
//  Weather
//
//  Created by Sergey Tszyu on 30/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

extension UIApplication {
    
    static func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}
