//
//  UINavigationBar+Extensions.swift
//  Weather
//
//  Created by Sergey Tszyu on 01/10/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func makeTransparent() {
        isTranslucent = true
        backgroundColor = .clear
        setBackgroundImage(UIImage(), for: .default)
        shadowImage = UIImage()
    }
    
    func makeOpaque() {
        isTranslucent = false
        backgroundColor = .white
        setBackgroundImage(nil, for: .default)
        shadowImage = UIImage()
    }
    
}
