//
//  UIView+Transformations.swift
//  Weather
//
//  Created by Sergey Tszyu on 28/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

protocol CornerProtocol: class {
    
    var corners: UIRectCorner { get set }
    var radius: CGFloat { get set }
    var maskLayer: CALayer { get set }
    
    var bounds: CGRect { get set }
    var layer: CALayer { get }
    
    func sizeToFit()
    
    func transformCorners()
    
}

extension CornerProtocol {
    
    func transformCorners() {
        let mask = UIBezierPath(roundedRect: bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = mask.cgPath
        layer.mask = maskLayer
    }
    
}

class CornerView: UIView, CornerProtocol {
    
    var corners: UIRectCorner = []
    
    var radius: CGFloat = 15
    
    var maskLayer: CALayer = CALayer()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        maskLayer.frame = bounds
        transformCorners()
    }
    
}

class CornerButton: UIButton, CornerProtocol {
    
    var corners: UIRectCorner = []
    
    var radius: CGFloat = 15
    
    var maskLayer: CALayer = CALayer()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        maskLayer.frame = bounds
        transformCorners()
    }
    
}
