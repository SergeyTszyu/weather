//
//  NavigationController.swift
//  Weather
//
//  Created by Sergey Tszyu on 01/10/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    
    // MARK: - Variables
    
    var isTransparentNavigationBar: Bool = false {
        didSet {
            if isTransparentNavigationBar {
                makeNavigationBarTransparent()
            } else {
                makeNavigationBarOpaque()
            }
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigationBarHidden(true, animated: animated)
    }
    
    // MARK: - Private
    
    func makeNavigationBarTransparent() {
        navigationBar.makeTransparent()
    }
    
    func makeNavigationBarOpaque() {
        navigationBar.makeOpaque()
    }
    
}
