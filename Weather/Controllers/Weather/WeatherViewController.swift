//
//  WeatherViewController.swift
//  Weather
//
//  Created by Sergey Tszyu on 29/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

final class WeatherViewController: UIViewController {
    
    // MARK: - @IBOutlets
    
    // MARK: - Properties
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

// MARK: - Private

private extension WeatherViewController {
    
}
