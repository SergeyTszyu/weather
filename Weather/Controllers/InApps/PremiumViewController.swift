//
//  PremiumViewController.swift
//  Weather
//
//  Created by Sergey Tszyu on 01/10/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

final class PremiumViewController: UIViewController {
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
