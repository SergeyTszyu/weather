//
//  PremiumIntroViewController.swift
//  Weather
//
//  Created by Sergey Tszyu on 01/10/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

final class PremiumIntroViewController: UIViewController {

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - @IBActions
    
    @IBAction func startTrial(_ sender: UIButton) {
        navigationController?.pushViewController(R.storyboard.weather.instantiateInitialViewController()!, animated: true)
    }
    
    @IBAction func restorePurchases(_ sender: UIButton) {
        
    }
    
    @IBAction func termsTapped(_ sender: UIButton) {
        
    }

}
