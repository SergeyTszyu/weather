//
//  LocationIntroViewController.swift
//  Weather
//
//  Created by Sergey Tszyu on 29/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

final class LocationIntroViewController: UIViewController {
    
    let manager = CLLocationManager()

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - @IBActions
    
    @IBAction func locationTapped(_ sender: UIButton) {
        if Defaults.isTutorialShown.boolValue {
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                case .denied:
                    Alert.showSettings(from: self)
                case .authorizedAlways, .authorizedWhenInUse:
                    let center = UNUserNotificationCenter.current()
                    center.getNotificationSettings { (settings) in
                        if (settings.authorizationStatus != .authorized) {
                           
                            DispatchQueue.main.async(execute: {
                                self.navigationController?.pushViewController(R.storyboard.intro.notificationsIntroViewController()!, animated: true)
                            })
                            
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.navigationController?.pushViewController(R.storyboard.inApps.premiumIntroViewController()!, animated: true)
                            })
                            
                        }
                    }
                default:
                    break
                }
            }
        } else {
            manager.delegate = self
            manager.requestAlwaysAuthorization()
            manager.startUpdatingLocation()
        }
    }
    
    @IBAction func notAllowTapped(_ sender: UIButton) {
        navigationController?.pushViewController(R.storyboard.intro.notificationsIntroViewController()!, animated: true)
    }
    
    @IBAction func termsTapped(_ sender: UIButton) {
        
    }
    
}

extension LocationIntroViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        navigationController?.pushViewController(R.storyboard.intro.notificationsIntroViewController()!, animated: true)
    }
}
