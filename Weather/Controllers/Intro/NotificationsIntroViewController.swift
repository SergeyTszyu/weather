//
//  NotificationsIntroViewController.swift
//  Weather
//
//  Created by Sergey Tszyu on 29/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit
import UserNotifications

final class NotificationsIntroViewController: UIViewController {
    
    let notificationCenter = UNUserNotificationCenter.current()

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - @IBActions
    
    @IBAction func notificationTapped(_ sender: UIButton) {
        
        guard !Defaults.isTutorialShown.boolValue else {
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings { (settings) in
                if (settings.authorizationStatus == .authorized) {
                    Defaults.notificationConfirmed.set(true)
                    DispatchQueue.main.async(execute: {
                        self.navigationController?.pushViewController(R.storyboard.inApps.premiumIntroViewController()!, animated: true)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        Alert.showNotificationSettings(from: self)
                    })
                }
            }
            return
        }
        
        notificationCenter.requestAuthorization(options: [.alert, .sound])
        { (granted, error) in
            if granted {
                Defaults.notificationConfirmed.set(true)
            }
            DispatchQueue.main.async(execute: {
                Defaults.isTutorialShown.set(true)
                self.navigationController?.pushViewController(R.storyboard.inApps.premiumIntroViewController()!, animated: true)
            })
        }
    }
    
    @IBAction func notAllowTapped(_ sender: UIButton) {
        Defaults.isTutorialShown.set(true)
        navigationController?.pushViewController(R.storyboard.inApps.premiumIntroViewController()!, animated: true)
    }
}

extension NotificationsIntroViewController: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
}
