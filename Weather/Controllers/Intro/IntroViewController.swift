//
//  IntroViewController.swift
//  Weather
//
//  Created by Sergey Tszyu on 29/09/2018.
//  Copyright © 2018 Sergey Tszyu. All rights reserved.
//

import UIKit

final class IntroViewController: UIViewController {

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - @IBActions
    
    @IBAction func continueTapped(_ sender: UIButton) {
        navigationController?.pushViewController(R.storyboard.intro.locationIntroViewController()!, animated: true)
    }
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
